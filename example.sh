#!/bin/bash

echo 'sampler_label,aggregate_report_count,average,,aggregate_report_min,aggregate_report_max,aggregate_report_90%_line,,aggregate_report_rate,aggregate_report_bandwidth,aggregate_report_error%' > columns-order.csv.tmp

echo 'sampler_label
empty line here
POST Add set to favorites
POST Comment
POST Like to scene
GET User Profile
GET Suggestions
GET Scene
GET Notifications
GET Feed
POST Scene
GET Scenes of specified user
GET Followers of user
GET Subscriptions of user
GET Notification count
GET Popular channels
GET Channel information
GET Scenes of channel' > rows-order.csv.tmp

echo 'sampler_label,aggregate_report_count,average,aggregate_report_median,aggregate_report_90%_line,aggregate_report_min,aggregate_report_max,aggregate_report_error%,aggregate_report_rate,aggregate_report_bandwidth
GET Notification count,282,286,41,948,3,2259,0.0,0.12058733730330794,0.02720113716638572
GET Popular channels,58,1781,803,4882,88,6104,0.0,0.0248098305103596,0.5246523896785972
GET User Profile,108,244,23,874,3,2547,0.0,0.0461488171160835,0.010451001199655592
GET Scenes of specified user,115,371,69,1247,4,2488,0.0,0.0491878866060957,0.013540452786258528
GET Channel information,72,1013,203,3633,14,4729,0.0,0.030805222854394836,0.04456620938203011
GET Scene,95,358,41,1038,4,2791,0.0,0.04063060408299079,0.02800454362422422
GET Feed,641,668,132,1950,9,4183,0.0,0.27412608944807004,1.842622012971809
GET Suggestions,37,770,221,1845,41,3238,0.0,0.015863229805036617,0.356910947364946
POST Like to scene,59,377,88,1227,5,2060,0.0,0.025249661376363322,0.004838794018398016
GET Scenes of channel,59,568,57,1739,12,3426,0.0,0.02524339553619753,0.12505416700538455
POST Comment,26,529,28,1667,5,2675,0.0,0.011121415054631814,0.0024002272725328427
GET Notifications,26,456,106,1175,33,2050,0.0,0.011178391653008964,0.24816037866909207
GET Subscriptions of user,47,218,38,790,3,1258,0.0,0.020151254458465048,0.005547373053528163
GET Followers of user,50,290,88,1108,4,1903,0.0,0.021388452802101204,0.005886419695984532
POST Scene,15,3021,2716,4412,1707,5483,0.0,0.006431692637126903,0.0011431328710518518
POST Add set to favorites,1,6,6,6,6,6,0.0,166.66666666666666,32.877604166666664
TOTAL,1691,572,102,1788,3,6104,0.0,0.7225606421090054,3.2359281875777683' > original.csv.tmp

node transform.js --rows-pattern rows-order.csv.tmp --columns-pattern columns-order.csv.tmp --original original.csv.tmp > destination.csv.tmp

cat destination.csv.tmp

rm *.csv.tmp
