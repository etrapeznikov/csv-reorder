var assert = require('chai').assert;
var tran = require('./transform');
var csv = require('csv');
var async = require('async');

describe('getFirstColumn', function () {
  it('should work on usual csv', function () {
    var rows = [
      [1, 'a', 'first'],
      [2, 'b', 'second'],
      [3, 'c', 'third']
    ];
    var result = tran.getFirstColumn(rows);
    assert.deepEqual(result, [1, 2, 3]);
  });
});

describe('applyPermutation', function () {
  it('should work adequately to getPermutation()', function () {
    var source = ['first', 'second', 'third', 'forth'];
    var dest = ['second', 'forth', 'third', 'first'];
    var permut = tran.getPermutation(source, dest);
    var permuted = tran.applyPermutation(source, permut);
    assert.deepEqual(permuted, dest);
  });
  it('should work adequately on increasing dimension', function () {
    var source = ['first', 'second', 'third', 'forth'];
    var dest = ['second', 'forth', 'third', 'first', 'first'];
    var permut = tran.getPermutation(source, dest);
    var permuted = tran.applyPermutation(source, permut);
    assert.deepEqual(permuted, dest);
  });
  it('should work adequately on decreasing dimension', function () {
    var source = ['first', 'second', 'third', 'forth'];
    var dest = ['second', 'forth', 'third'];
    var permut = tran.getPermutation(source, dest);
    var permuted = tran.applyPermutation(source, permut);
    assert.deepEqual(permuted, dest);
  });
  it('should allow missing elements', function () {
    var source = ['first', 'second', 'third', 'forth'];
    var dest = ['second', 'forth', 'third'];
    var permut = tran.getPermutation(source, dest);
    var permuted = tran.applyPermutation(source, permut);
    assert.deepEqual(permuted, dest);
  });
  it('should allow unknown elements', function () {
    var source = ['first', 'second', 'third', 'forth'];
    var dest = ['second', 'forth', 'unknown', 'third'];
    var dest2 = ['second', 'forth', undefined, 'third'];
    var permut = tran.getPermutation(source, dest);
    var permuted = tran.applyPermutation(source, permut);
    assert.deepEqual(permuted, dest2);
  });
});

describe('reorderRows', function () {
  it('should work on simple input', function () {
    var orig = [
      [1, 'a', 'first'],
      [2, 'b', 'second'],
      [3, 'c', 'third']
    ];
    var pattern = [
      [2],
      [1],
      [3]
    ];
    var result = tran.reorderRows(orig, pattern);
    assert.deepEqual(result, [
      [2, 'b', 'second'],
      [1, 'a', 'first'],
      [3, 'c', 'third']
    ]);
  });
  it('should allow dimension increasing', function () {
    var orig = [
      [1, 'a', 'first'],
      [2, 'b', 'second'],
      [3, 'c', 'third']
    ];
    var pattern = [
      [2],
      [1],
      [1], 
      [3]
    ];
    var result = tran.reorderRows(orig, pattern);
    assert.deepEqual(result, [
      [2, 'b', 'second'],
      [1, 'a', 'first'],
      [1, 'a', 'first'],
      [3, 'c', 'third']
    ]);
  });
  it('should allow dimension decreasing', function () {
    var orig = [
      [1, 'a', 'first'],
      [2, 'b', 'second'],
      [3, 'c', 'third']
    ];
    var pattern = [
      [2],
      [3]
    ];
    var result = tran.reorderRows(orig, pattern);
    assert.deepEqual(result, [
      [2, 'b', 'second'],
      [3, 'c', 'third']
    ]);
  });
  it('should allow unknown rows', function () {
    var orig = [
      [1, 'a', 'first'],
      [2, 'b', 'second'],
      [3, 'c', 'third']
    ];
    var pattern = [
      [1],
      [2],
      ['unknown'],
      [3]
    ];
    var result = tran.reorderRows(orig, pattern);
    assert.deepEqual(result, [
      [1, 'a', 'first'],
      [2, 'b', 'second'],
      [],
      [3, 'c', 'third']
    ]);
  });
});

describe('reorderColumns', function () {
  it('should work on simple input', function () {
    var orig = [
      [1, 'a', 'first'],
      [2, 'b', 'second'],
      [3, 'c', 'third']
    ];
    var pattern = [
      ['first', 1, 'a']
    ];
    var result = tran.reorderColumns(orig, pattern);
    assert.deepEqual(result, [
      ['first', 1, 'a'],
      ['second', 2, 'b'],
      ['third', 3, 'c']
    ]);
  });
  it('should allow dimension decreasing', function () {
    var orig = [
      [1, 'a', 'first'],
      [2, 'b', 'second'],
      [3, 'c', 'third']
    ];
    var pattern = [
      ['first', 'a']
    ];
    var result = tran.reorderColumns(orig, pattern);
    assert.deepEqual(result, [
      ['first', 'a'],
      ['second', 'b'],
      ['third', 'c']
    ]);
  });
  it('should allow dimension increasing', function () {
    var orig = [
      [1, 'a', 'first'],
      [2, 'b', 'second'],
      [3, 'c', 'third']
    ];
    var pattern = [
      ['first', 'first', 'first', 'a']
    ];
    var result = tran.reorderColumns(orig, pattern);
    assert.deepEqual(result, [
      ['first', 'first', 'first', 'a'],
      ['second','second','second', 'b'],
      ['third','third','third', 'c']
    ]);
  });
  it('should do padding', function () {
    var orig = [
      [1, 'a', 'first'],
      [2, 'b', 'second'],
      [3, 'c', 'third']
    ];
    var pattern = [
      ['first',,, 1, 'a']
    ];
    var result = tran.reorderColumns(orig, pattern);
    assert.deepEqual(result, [
      ['first',,, 1, 'a'],
      ['second',,, 2, 'b'],
      ['third',,, 3, 'c']
    ]);
  });
  it('should allow empty columns', function () {
    var orig = [
      [1,,,, 'a', 'first'],
      [2,,,, 'b', 'second'],
      [3,,,, 'c', 'third']
    ];
    var pattern = [
      ['first', 1, 'a']
    ];
    var result = tran.reorderColumns(orig, pattern);
    assert.deepEqual(result, [
      ['first', 1, 'a'],
      ['second', 2, 'b'],
      ['third', 3, 'c']
    ]);
  });
});

describe('reorderBoth', function () {
  it('should work on simple input', function () {
    var orig = [
      [1, 'a', 'first'],
      [2, 'b', 'second'],
      [3, 'c', 'third']
    ];
    var columnsPattern = [
      ['first', 1, 'a'],
    ];
    var rowsPattern = [
      [3], 
      [2], 
      [1]
    ];
    var result = tran.reorderBoth(orig, rowsPattern, columnsPattern);
    assert.deepEqual(result, [
      ['third', 3, 'c'],
      ['second', 2, 'b'],
      ['first', 1, 'a'],
    ]);
  });
});
