var csv = require('csv');
var fs = require('fs');
var optimist = require('optimist');
var format = require('util').format;
var async = require('async');
var assert = require('chai').assert;

// build permutation xs→ by by xs, ys
function getPermutation(xs, ys) {
  return ys.map(function (value) {
    return xs.indexOf(value); 
  });
}

// apply permutatin to array
function applyPermutation(arr, perm, defaultValue) {
  return perm.map(function (destIndex) {
    return arr[destIndex] || defaultValue;
  });
}

// get permutation of rows
function getRowsPermutation(original, pattern) {
  var originalOrder = getFirstColumn(original);
  var patternOrder = getFirstColumn(pattern);
  return getPermutation(originalOrder, patternOrder);
}

// apply permutation to rows
function applyRowsPermutation(original, permut) {
  assert.isArray(original, 'original must be array of rows');
  return applyPermutation(original, permut, []);
}

// get permutation of columns
function getColumnsPermutation(original, pattern) {
  assert.property(original, 0, 'original must not be empty');
  assert.property(pattern, 0, 'pattern must not be empty');
  return getPermutation(original[0], pattern[0]);
}

// apply permutation to columns
function applyColumnsPermutation(original, permut) {
  return original.map(function (row) {
    assert.isArray(row, 'each row of original must be an array');
    return applyPermutation(row, permut, null); 
  });
}

// change rows order of original to be the same as in pattern
function reorderRows(original, pattern) {
  var permut = getRowsPermutation(original, pattern);
  return applyRowsPermutation(original, permut);
}

// change columns order of original to be the same as in pattern
function reorderColumns(original, pattern) {
  var permut = getColumnsPermutation(original, pattern);
  return applyColumnsPermutation(original, permut);
}

// return first elements of each row
function getFirstColumn(rows) {
  return rows.map(function (row) {
    assert.property(row, 0, 'each row must not be empty');
    return row[0];
  });
}

// change both rows and columns order of original to be the same as in pattern
function reorderBoth(original, rowsPattern, columnsPattern) {
  var rowsPermut = getRowsPermutation(original, rowsPattern);
  var columnsPermut = getColumnsPermutation(original, columnsPattern);
  var originalButRows = applyRowsPermutation(original, rowsPermut);
  return applyColumnsPermutation(originalButRows, columnsPermut);
}

if (require.main == module) {
  var argv = optimist
    .usage('Usage: $0 option [OUTPUT]')
    .describe('rows-pattern', 'Set rows order pattern file')
    .describe('columns-pattern', 'Set columns order pattern file')
    .describe('original', 'Set original file')
    .demand('original').argv;

  if (!argv.original) {
    argv.showHelp();
    process.exit();
  }


  var originalPath = argv.original;
  var rowsPatternPath = argv['rows-pattern'] || originalPath;
  var columnsPatternPath = argv['columns-pattern'] || originalPath;

  var originalCsv = csv().from.path(originalPath);
  var rowsPatternCsv = csv().from.path(rowsPatternPath);
  var columnsPatternCsv = csv().from.path(columnsPatternPath);

  async.auto({
    original: function (next) {
      originalCsv.to.array(function (arr) {
        next(null, arr);  
      });
    },
    rowsPattern: function (next) {
      rowsPatternCsv.to.array(function (arr) {
        next(null, arr);  
      });
    },
    columnsPattern: function (next) {
      columnsPatternCsv.to.array(function (arr) {
        next(null, arr);  
      });
    },
    null: [
      'original',
      'rowsPattern',
      'columnsPattern',
      function (next, results) {
        var target = reorderBoth(results.original, results.rowsPattern, results.columnsPattern);
        var targetCsv = csv().from.array(target);
        if (argv._[0]) {
          targetCsv.to.path(argv._[0]);
        } else {
          targetCsv.to.stream(process.stdout);
        }
      }
    ]
  });
}

exports.getFirstColumn = getFirstColumn;
exports.applyPermutation = applyPermutation;
exports.getPermutation = getPermutation;
exports.reorderRows = reorderRows;
exports.reorderColumns = reorderColumns;
exports.reorderBoth = reorderBoth;
